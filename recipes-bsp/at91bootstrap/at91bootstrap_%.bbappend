PROVIDES = "virtual/bootloader"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"
PR .= ".0"
COMPATIBLE_MACHINE = "(sama5d3-acqua*)"

AT91BOOTSTRAP_TARGET = "${AT91BOOTSTRAP_CONFIG}_defconfig"

S = "${WORKDIR}/git"
SRC_URI = "git://github.com/linux4sam/at91bootstrap.git;protocol=git;branch=master \
"
SRCREV = "e63c7c2613fe1d0cfb2372b27577b9ad3d9eadb0"

do_configure() {
        
	echo "${S}/contrib/board/acme/${AT91BOOTSTRAP_MACHINE}/${AT91BOOTSTRAP_TARGET}"
        cp ${S}/contrib/board/acme/${AT91BOOTSTRAP_MACHINE}/${AT91BOOTSTRAP_TARGET} ${B}/.config
        #cp ${S}/contrib/board/acme/${AT91BOOTSTRAP_MACHINE}/Config* ${B}

        cml1_do_configure
}

do_compile() {

        unset CFLAGS CPPFLAGS LDFLAGS
        oe_runmake ${AT91BOOTSTRAP_TARGET}
        oe_runmake
}

