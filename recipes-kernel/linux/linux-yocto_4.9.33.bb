KBRANCH ?= "standard/base"

require recipes-kernel/linux/linux-yocto.inc

SRCREV_machine ?= "050639ef5810e8ad17fb6a426eff3c63e616350c"

SRC_URI = "git://git.yoctoproject.org/linux-yocto-4.9.git;name=machine;branch=${KBRANCH}"

DEPENDS += "openssl-native util-linux-native"

LINUX_VERSION ?= "4.9.33"

PV = "${LINUX_VERSION}+git${SRCPV}"

KMETA = "kernel-meta"
KCONF_BSP_AUDIT_LEVEL = "2"

FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "\
    file://defconfig \
    file://linux-4.9.33.patch \
"
KERNEL_DEVICETREE = " \
		acme-acqua.dtb \
                "

