require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;branch=linux-4.9.y"

SRC_URI += "\
    file://defconfig \
    file://linux-4.9.33.patch \
"
KERNEL_DEVICETREE = " \
		acme-acqua.dtb \
                "
SRCREV ?= "050639ef5810e8ad17fb6a426eff3c63e616350c"

LINUX_VERSION ?= "4.9.33"
PV = "${LINUX_VERSION}"
